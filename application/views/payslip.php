<style type="text/css">
  .bg-blue{
   background-color:#007bff ;
    color: white !important;

    font-size: 18px;


  }
    .bg-b{
    background-color: rgb(5,111,197);
    color: white;

    font-size: 13px;

  }

  .no-bg{
    background-color:#25e02a;
    color: black;
  }

</style>

<?php 
  
  $ttlearn= 0;
  $ttldeduc = 0;
  $netin = 0;

 ?>


<div class="container print">


     <div class="card">
      <div class="card-header bg-primary bg-b"><h1><img src="<?php echo base_url(); ?>assets/image/logowhite.svg" height="4%" width="4%">&nbspPayslip<button style="float: right;" onclick="myFunction()" class="btn btn-success no-print">Print <i class="fas fa-print"></i>
</button></h1>
      </div>
      <div class="card-body">

        <div class="row">
          <div class="col-sm-6">
            <div class="card card-primary">
              <div class="card-heading bg-primary bg-b"><h3 align="center">Employee Pay Summary</h3></div>
               <div class="card-body">

  <table class="table bg-blue">
  <tbody>
    <?php foreach ($value1 as $row): ?>      
    <tr>
      <td class= "print">Employee Name</td>
      <td class= "print">:</td>
        <td class= "print"><?php echo $row['firstname']. " " .$row['lastname']; ?></td>
    </tr>
    <tr>
      <td class= "print">Employee ID</td>
      <td class= "print">:</td>
       <td class= "print"><?php echo $row['employeeid']; ?></td>
    </tr>
    <tr>
      <td class= "print">Hourly Rate</td>
      <td class= "print">:</td>
      <td class= "print"><?php echo $row['hourlyrate']; ?></td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>


      </div>
    </div>

    <div class="card">
      <div class="card-header bg-primary bg-b"><h3>Earnings</h3></div>
      <div class="card-body">


          <table class="table bg-blue">
  <tbody>
    <?php foreach ($value2 as $row2): ?>
    <tr>
      <td class= "print">Basic Pay</td>
      <td class= "print">:</td>
      <td class= "print"><?php echo $row2['basicpay']; $ttlearn = $ttlearn+$row2['basicpay'] ?></td>
    </tr>
    <tr>
      <td class= "print">Holiday Pay</td>
      <td class= "print">:</td>
      <td class= "print"><?php echo $row2['holidaypay']; $ttlearn = $ttlearn+$row2['holidaypay'] ?></td>

    </tr>
    <tr>
      <td class= "print">Regular OT</td>
      <td class= "print">:</td>
      <td class= "print"><?php echo $row2['regot']; $ttlearn = $ttlearn+$row2['regot'] ?></td>

    </tr>
    <tr>
      <td class= "print">6th Day OT</td>
      <td class= "print">:</td>
      <td class= "print"><?php echo $row2['6thdayot']; $ttlearn = $ttlearn+$row2['6thdayot'] ?></td>

    </tr>
    <tr>
      <td class= "print">7th Day OT</td>
      <td class= "print">:</td>
      <td class= "print"><?php echo $row2['7thdayot']; $ttlearn = $ttlearn+$row2['7thdayot'] ?></td>

    </tr>
    <tr>
      <td class= "print">Total ND</td>
      <td class= "print">:</td>
      <td class= "print"><?php echo $row2['totalnd']; $ttlearn = $ttlearn+$row2['totalnd'] ?></td>

    </tr>
      <?php if(!empty($value4)): ?>
      <?php foreach($value4 as $row4): ?>
    <tr>

  <td class="print"><?php echo $row4['description']; ?></td>
  <td class="print">:</td>
  <td class="print"><?php echo $row4['amount']; $ttlearn = $ttlearn+$row4['amount'] ?></td>
  </tr>
    <?php endforeach; ?>
  <?php else: ?>
    <tr>
    </tr>
  <?php endif; ?>
  <?php if(!empty($value3)): ?>
        <?php foreach ($value3 as $row3): ?>
  <tr>

      <td class="print"><?php echo $row3['description']; ?></td>
      <td class="print">:</td>
    <td class="print"><?php echo $row3['amount']; $ttlearn = $ttlearn+$row3['amount'] ?></td>
  </tr>
    <?php endforeach; ?>
  <?php else: ?>
    <tr>
    </tr>
  <?php endif; ?>

    <tr>
      <td class="print no-bg"><STRONG>Total Earning</STRONG></td>
      <td class="print no-bg">:</td>
      <td class="print no-bg"><STRONG><?php echo $ttlearn; ?></STRONG></td>
  </tr>
  </tbody>
</table>
      </div>
    </div>
    </div>



          <div class="col-sm-6">

          <div class="card">
            <div class="card-header bg-primary bg-b"><h2>Payslip For Pay Period:</h2></div>
              <div class="card-body">
                  
                  <STRONG><?php echo $row2['payperiod']; ?></STRONG>

              </div>
            </div>
            


      <div class="card">
      <div class="card-header bg-primary bg-b"><h4>Deductions</h4></div>
      <div class="card-body">

  <table class="table bg-blue">
  <tbody>
    <tr>
      <td class= "print">Late/Absences/Others</td>
      <td class= "print">:</td>
      <td class= "print"><?php echo $row2['deductions']; $ttldeduc = $ttldeduc+$row2['deductions'] ?></td>

    </tr>
    <tr>
      <td class= "print">Loan Deduction</td>
      <td class= "print">:</td>
      <td class= "print"><?php echo $row2['loandeductions']; $ttldeduc = $ttldeduc+$row2['loandeductions'] ?></td>
    </tr>
    <tr>
      <td class= "print">With Holding Tax</td>
      <td class= "print">:</td>
       <td class= "print"><?php echo $row2['tax']; $ttldeduc = $ttldeduc+$row2['tax'] ?></td>
    </tr>
    <tr>
      <td class= "print">SSS</td>
      <td class= "print">:</td>
      <td class= "print"><?php echo $row2['sss']; $ttldeduc = $ttldeduc+$row2['sss'] ?></td>

    </tr>
     <tr>
      <td class= "print">PHIC</td>
      <td class= "print">:</td>
      <td class= "print"><?php echo $row2['phic']; $ttldeduc = $ttldeduc+$row2['phic'] ?></td>

    </tr>
     <tr>
      <td class= "print">HDMF</td>
      <td class= "print">:</td>
      <td class= "print"><?php echo $row2['hdmf']; $ttldeduc = $ttldeduc+$row2['hdmf']  ?></td>
    </tr>

    </tr>
     <tr>
      <td class= "print no-bg"><strong>Total Deduction</strong></td>
      <td class= "print no-bg">:</td>
      <td class= "print no-bg"><strong><?php echo $ttldeduc; ?></strong></td>
    </tr>

    <tr>
          <td colspan="3">
            <center>
              <a class="btn btn-success" data-toggle="collapse" href="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Late/Absences/Other Details</a>
            </center>
          </td>
    </tr>
    <tr>

  <td colspan="3">
<div class="row">
  <div class="col">
    <div class="collapse multi-collapse" id="multiCollapseExample2">
        <table class="bg-blue">
          <tbody>
            <?php if(!empty($value5)): ?>
              <?php foreach($value5 as $row5): ?>
            <tr>
              <td class="print"><?php echo $row5['description']; ?></td>
              <td class="print">:</td>
              <td class="print"><?php echo $row5['amount']; ?></td>
            </tr>
          <?php endforeach; ?>
          <?php else: ?>
          <?php endif; ?>
          <tr>

                <?php if(!empty($value6)): ?>
              <?php foreach($value6 as $row6): ?>

              <td class="print"><?php echo $row6['loantype']; ?></td>
              <td class="print">:</td>
              <td class="print"><?php echo $row6['loanamt']; ?></td>

          <?php endforeach; ?>
          <?php else: ?>
          <?php endif; ?>

            


          </tr>

           <tr>

                <?php if(!empty($value7)): ?>
              <?php foreach($value7 as $row7): ?>

              <td class="print"><?php echo $row7['description']; ?></td>
              <td class="print">:</td>
              <td class="print"><?php echo $row7['amount']; ?></td>
              
          <?php endforeach; ?>
          <?php else: ?>
          <?php endif; ?>

            


          </tr>

          </tbody>
        </table>
    </div>
  </div>
</div>

</td>
      
    </tr>



  </tbody>
</table>

      </div>
    </div>

          <div class="card">
      <div class="card-header bg-primary bg-b"><h4>Net Pay Summary</h4></div>
      <div class="card-body">

  <table class="table bg-blue">
  <tbody>
    <tr>
      <td class= "print">Total Earning</td>
      <td class= "print">:</td>
      <td class= "print"><?php echo $ttlearn; ?></td>

    </tr>
    <tr>
      <td class= "print">Total Deduction</td>
      <td class= "print">:</td>
       <td class= "print"><?php echo $ttldeduc; ?></td>
    </tr>
    <tr>
      <td class= "print">Net Income</td>
      <td class= "print">:</td>
      <td class= "print"><strong><?php echo $row2['netpay']; ?></strong></td>

    </tr>
  </tbody>

<?php endforeach; ?>
</table>

      </div>
    </div>





    </div>


<script>
function myFunction() {
  window.print();
}
</script>